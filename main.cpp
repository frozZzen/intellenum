#include "enum_class.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cassert>

struct Example
{
  enum_class (State,
    IDLE = 1,
    STARTUP_INIT = 2 * IDLE,
    STARTUP = IDLE - 1,
    STARTED = (IDLE + STARTUP_INIT) << 2,
    SUSPENDED,
    STOPPED,
    ERROR = 99,
    INIT = STARTED
  );
};

int main()
{
  using State = Example::State;
  
  //list the name of the enum items
  std::cout << "enum class State::items\n{\n";
  for (auto i : Example::State())
  {
    std::cout << "  " << i.second << ",\n";
  }
  std::cout << "};" << std::endl << std::endl;
  
  //we look up the enum item by its name and then check the name returned for that item if it's the same
  const char* name = "STARTUP";
  const char* looked_up_name = Example::State::to_string(Example::State::to_item(name));
  bool is_same = strcmp(name, looked_up_name) == 0;
  std::cout << "Checking lookup of " << name << ": " << name << (is_same ? " == " : " != ") << looked_up_name << std::endl << std::endl;
  assert(is_same);
  
  //we look up the longest enum item name with and without the initialization expression
  auto nameLengthComparator = [](const auto& i1, const auto& i2) { return strlen(State::to_string(i1.first)) < strlen(State::to_string(i2.first)); };
  size_t max = strlen(State::to_string(std::max_element(Example::State().begin(), Example::State().end(), nameLengthComparator)->first));
  auto fullNameLengthComparator = [](const auto& i1, const auto& i2) { return strlen(i1.second) < strlen(i2.second); };
  size_t maxf = strlen(std::max_element(Example::State().begin(), Example::State().end(), fullNameLengthComparator)->second);
  
  //display the names with and without initialization expression for each individual enum item with correct padding
  std::cout << std::setw(max) << std::left << "Name by value:" << "\t" << std::setw(maxf) << "Item expression:" << "\tItem value:" << std::endl;
  for (const auto& i : Example::State())
  {
    std::cout << std::setw(max) << std::left << State::to_string(i.first) << "\t" << std::setw(maxf) << i.second << "\t" << static_cast<int>(i.first) << std::endl;
  }
  
  //return name from full string
  const char* parsed_name = Example::State::parse_name("STARTUP = IDLE - 1");
  is_same = strcmp(name, parsed_name) == 0;
  std::cout << std::endl << "Checking name parsed from full string: " << name << (is_same ? " == " : " != ") << parsed_name << std::endl;
  assert(is_same);
  
  return 0;
}

/* PROGRAM OUTPUT:
enum class State::items
{
  IDLE = 1,
  STARTUP_INIT = 2 * IDLE,
  STARTUP = IDLE - 1,
  STARTED = (IDLE + STARTUP_INIT) << 2,
  SUSPENDED,
  STOPPED,
  ERROR = 99,
  INIT = STARTED,
};

Checking lookup of STARTUP: STARTUP == STARTUP

Name by value:  Item expression:                      Item value:
IDLE            IDLE = 1                              1
STARTUP_INIT    STARTUP_INIT = 2 * IDLE               2
STARTUP         STARTUP = IDLE - 1                    0
STARTED         STARTED = (IDLE + STARTUP_INIT) << 2  12
SUSPENDED       SUSPENDED                             13
STOPPED         STOPPED                               14
ERROR           ERROR = 99                            99
STARTED         INIT = STARTED                        12

Checking name parsed from full string: STARTUP == STARTUP
*/
