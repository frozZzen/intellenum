/* Copyright 2017 frozZzen (https://bitbucket.org/frozZzen)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
#ifndef enum_class_h
#define enum_class_h

#include <boost/preprocessor.hpp>
#include <cstring>
#include <cctype>
#include <stdexcept>
#include <iterator>
#include <utility>

/**
 * Macro: enum_class
 * 
 * Creates an intelligent enum that provides facilities to iterate through the enum items, convert
 * them to strings back and forth, check that an int value can be safely casted to an enum item.
 * 
 * Interface:
 * 
 *   static constexpr int size()                             - Returns the number of enum items.
 *   static const char* to_string(items item)                - Returns the name of the enum item.
 *   static constexpr const char* to_string_full(items item) - Returns the name and the initialization
 *                                                             expression of the enum item.
 *   static const char* parse_name(const char* full_string)  - Returns the name of the enum item from its
 *                                                             full expression that includes initialization
 *   static bool to_item(const char* str, items& item)       - Converts an enum item name to enum item
 *                                                             and returns if the conversion was successful.
 *   static items to_item(const char* str)                   - Converts an enum item name to enum item
 *                                                             and throws exception if it was unsuccessful.
 *   static items to_item(int item)                          - Converts an enum item value to enum item
 *                                                             and throws exception if it was unsuccessful.
 *   static constexpr bool has_item(int item)                - Returns true if the enum item value is valid.
 *   iterator begin() const                                  - Returns a bidirectional iterator that points
 *                                                             to the first enum item.
 *   iterator end() const                                    - Returns the end iterator.
 *   std::pair<items, const char*> iterator::operator*()     - Dereferencing the iterator returns an
 *                                                             std::pair containing the item value and name
 * 
 * The underlying structure is the following:
 *   struct my_type_name { enum class items { ... } };
 * 
 * Usage:
 * 
 *   enum_class (my_type_name,
 *     MY_ITEM_1,
 *     MY_ITEM_2 = 4,
 *     MY_ITEM_3 = 1 << 16,
 *     MY_ITEM_4 = (1 + MY_ITEM_3) * MY_ITEM_2 % MY_ITEM_1
 *   );
 * 
 *   ...
 *   my_type_name::items i = my_type_name::to_item(4);
 *   if (i == my_type_name::items::MY_ITEM_1)
 *   ...
 *   for (auto item : my_type_name())
 *   {
 *     std::cout << "name = " << item->second << ", value = " << static_cast<int>(item->first) << std::endl;
 *   }
 *   ...
 */
#define enum_class_INTERNAL_TRANSFORM_TO_STR_OP(d, data, elem) BOOST_PP_STRINGIZE(elem)
#define enum_class(type_name, ...) \
  class type_name final \
  { \
  public: \
    enum class items : int { __VA_ARGS__ }; \
    \
    static constexpr int size() \
    { \
      struct dummy { int __VA_ARGS__; }; \
      return sizeof(dummy) / sizeof(int); \
    }; \
    static const char* to_string(items item) \
    { \
      return parse_name(to_string_full(item)); \
    } \
    static constexpr const char* to_string_full(items item) \
    { \
      value_capture __VA_ARGS__; \
      /*there will be initialized and uninitialized value_capture objects
        based on whether the enum item expression contains an initialization
        or not; for example: value_capture array[] = { MY_ITEM_1,
        MY_ITEM_2 = 1 << 3 }; in this case MY_ITEM_1 is an uninitialized object
        (MY_ITEM_1.inited == false, MY_ITEM_1.value == 0) and MY_ITEM_2 is an
        initialized one (MY_ITEM_2.inited == true, MY_ITEM_2.value == 8)*/ \
      value_capture array[] = { __VA_ARGS__ }; \
      const int a_size = size(); \
      calculate_value_capture_list(array, a_size); \
      const char* ptr2inited = nullptr; \
      \
      /*we iterate through the value_capture object list
        and we look up the specified item; if we found such
        an object that was not initialized (its inited value
        is false) and its value matches the specified one, then we
        immediately return the corresponding string, otherwise,
        if no such objects were found, but there was an initialized
        object of which value matches the specified one, then we
        return the corresponding string of that value; if none of
        the previous cases occur, we return an empty string*/ \
      for (int i = 0; i < a_size; ++i) \
      { \
        if (array[i].value == static_cast<int>(item)) \
        { \
          if (!array[i].inited) \
          { \
            return get_nth_item_str(i); \
          } \
          else if (ptr2inited == nullptr) \
          { \
            ptr2inited = get_nth_item_str(i); \
          } \
        } \
      } \
      return ptr2inited != nullptr ? ptr2inited : ""; \
    } \
    static const char* parse_name(const char* full_string) \
    { \
      static thread_local char buffer[128] = ""; \
      size_t pos = 0; \
      /*pos will be the length of the enum item name*/ \
      while (isalnum(full_string[pos]) || full_string[pos] == '_') ++pos; \
      /*copy only the name and close it with a terminating null char*/ \
      strncpy(buffer, full_string, pos <= 128 ? pos : 128); \
      buffer[pos] = '\0'; \
      return buffer; \
    } \
    static bool to_item(const char* name, items& item) \
    { \
      const int a_size = size(); \
      for (int i = 0; i < a_size; ++i) \
      { \
        size_t pos = 0; \
        /*we increase pos until we get to the end of the enum item name*/ \
        while (isalnum(name[pos]) || name[pos] == '_') ++pos; \
        /*if the next character either in the specified name or in the checked one is the continuation of the name,
          then the two names don't match*/ \
        if (isalnum(name[pos]) || name[pos] == '_' || isalnum(get_nth_item_str(i)[pos]) || get_nth_item_str(i)[pos] == '_') \
        { \
          continue; \
        } \
        if (strncmp(name, get_nth_item_str(i), pos) == 0) \
        { \
          item = static_cast<items>(get_nth_item_value(i)); \
          return true; \
        } \
      } \
      return false; \
    } \
    static items to_item(const char* name) \
    { \
      items item; \
      if (to_item(name, item)) return item; \
      throw std::invalid_argument("Invalid string"); \
    } \
    static items to_item(int item) \
    { \
      if (has_item(item)) return static_cast<items>(item); \
      throw std::invalid_argument("Invalid integer"); \
    } \
    static constexpr bool has_item(int item) \
    { \
      const int a_size = size(); \
      for (int i = 0; i < a_size; ++i) \
      { \
        if (item == get_nth_item_value(i)) return true; \
      } \
      return false; \
    } \
    \
    class iterator : public std::bidirectional_iterator_tag \
    { \
    public: \
      iterator() : index(size())                    { } \
      bool operator==(const iterator& other) const  { return this->index == other.index; } \
      bool operator!=(const iterator& other) const  { return this->index != other.index; } \
      iterator& operator++()                        { ++index; return *this; } \
      iterator operator++(int)                      { iterator current = *this; operator++(); return current; } \
      iterator& operator--()                        { --index; return *this; } \
      iterator operator--(int)                      { iterator current = *this; operator--(); return current; } \
      std::pair<items, const char*> operator*() const \
      { \
        if (index >= size() || index < 0) throw std::out_of_range("Invalid iterator"); \
        return {static_cast<items>(get_nth_item_value(index)), get_nth_item_str(index)}; \
      } \
      class pair_holder \
      { \
        /*this class is a bridge to keep the std::pair returned by operator*() alive while
          calling operator->() for simpler access to the members of std::pair*/ \
        friend class iterator; \
        constexpr pair_holder(std::pair<items, const char*>&& pair) : pair(std::move(pair)) { } \
        std::pair<items, const char*> pair; \
      public: \
        constexpr std::pair<items, const char*>* operator->() { return &pair; } \
      }; \
      pair_holder operator->() \
      { \
        return pair_holder{operator*()}; \
      } \
    private: \
      friend class type_name; \
      iterator(int index) : index(index)      { } \
      int index; \
    }; \
    \
    iterator begin() const  { return iterator(0); } \
    iterator end() const    { return iterator(size()); } \
    \
  private: \
    friend class iterator; \
    struct value_capture \
    { \
      constexpr value_capture() : value(0), inited(false) 	  { } \
      constexpr value_capture(int i) : value(i), inited(true) { } \
      constexpr value_capture& operator=(int i)               { this->value = i; inited = true; return *this; } \
      constexpr value_capture operator+()                     { return value_capture(+value); } \
      constexpr value_capture operator-()                     { return value_capture(-value); } \
      constexpr value_capture operator+(int i)                { return value_capture(value + i); } \
      constexpr value_capture operator-(int i)                { return value_capture(value - i); } \
      constexpr value_capture operator*(int i)                { return value_capture(value * i); } \
      constexpr value_capture operator/(int i)                { return value_capture(value / i); } \
      constexpr value_capture operator%(int i)                { return value_capture(value % i); } \
      constexpr value_capture operator~()                     { return value_capture(~value); } \
      constexpr value_capture operator&(int i)                { return value_capture(value & i); } \
      constexpr value_capture operator|(int i)                { return value_capture(value | i); } \
      constexpr value_capture operator^(int i)                { return value_capture(value ^ i); } \
      constexpr value_capture operator<<(int i)               { return value_capture(value << i); } \
      constexpr value_capture operator>>(int i)               { return value_capture(value >> i); } \
      constexpr operator int()                                { return value; } \
      int value; \
      bool inited; \
    }; \
    \
    static constexpr int get_nth_item_value(int n) \
    { \
      value_capture __VA_ARGS__; \
      value_capture array[] = { __VA_ARGS__ }; \
      const int a_size = size(); \
      calculate_value_capture_list(array, a_size); \
      return array[n].value; \
    } \
    static constexpr const char* get_nth_item_str(int n) \
    { \
      const int a_size = size(); \
      /*we create an array of strings that will contain the enum item names with their initialization expressions*/ \
      const char* strmap[a_size] = { BOOST_PP_LIST_ENUM( \
        BOOST_PP_LIST_TRANSFORM(enum_class_INTERNAL_TRANSFORM_TO_STR_OP, type_name, BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__))) }; \
      return strmap[n]; \
    } \
    static constexpr void calculate_value_capture_list(value_capture* array, int size) \
    { \
      /*we have to iterate over the list of value_capture objects and set the values
        for the uninitialized objects by adding one to the value of the previous object*/ \
      for (int i = 0; i < size; ++i) \
      { \
        if (!array[i].inited && i > 0) \
        { \
          array[i].value = array[i - 1].value + 1; \
        } \
      } \
    } \
  }
  
#endif //enum_class_h
